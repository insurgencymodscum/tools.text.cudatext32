Plugin for CudaText.
Gives commands in menu "Plugins" to manage sessions (session is a set of opened named tabs, with props of each tab).

Commands:
  Recent sessions      Show previous sessions list and open selected
  Open/New session     Show "Open" dialog to open session file (create session if filename not exists)
  Open previous        Open previous session (from recent list)
  Save session         Save current session (CudaText does it only on close)
  Save session as      Save current session to new file

Author: Andrey Kvichanskiy (kvichans, at forum)
