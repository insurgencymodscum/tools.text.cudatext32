@echo off

rem Set the current directory to the location of the batch script, using the %0 parameter
pushd "%~dp0"

rem Start an application without creating a new window.
start /b cudatext.exe

rem Change directory back to the path/folder most recently stored by the PUSHD command.
popd